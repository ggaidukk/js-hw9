1. document.createElement("тег") 
2. position

DOMString - визначає позицію елемента, що додається, щодо елемента, який викликав метод. Має відповідати одному з таких значень (чутливо до регістру):

'beforebegin': до самого element (до відкриваючого тега).
'afterbegin': одразу після відкриваючого тега element (перед першим нащадком).
'beforeend': одразу перед закриваючим тегом element (після останнього нащадка).
'afterend': після element (після закриваючого тега).
3. Cпочатку потрібно вибрати елемент для видалення, а потім просто застосувати метод remove()